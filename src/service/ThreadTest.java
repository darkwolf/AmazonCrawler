package service;

import java.util.ArrayList;
import java.util.List;

public class ThreadTest{
	private static List<Thread> threadPool = new ArrayList<>();
	private int threadNumber = 0;
	public static void main(String[] args) {
		new ThreadTest().testthread();
	}
	public void testthread() {
		for (int i = 0; i < 30; i++) {
			threadPool.add(new Thread(new Runnable() {
				@Override
				public void run() {
					synchronized (this) {
						threadNumber+=1;
						System.out.println(threadNumber);
						if (threadNumber < threadPool.size()) {
							threadPool.get(threadNumber).start();
						}
						else if (threadNumber == threadPool.size()){
							System.out.println("完成，导出");
							return;
						}
					}
				}
			}));
		}
		threadPool.get(0).start();
		int i = 0;
		while (true) {
			System.out.println(threadPool.get(i).isAlive());
			System.out.println("i=" + i);
			if (!threadPool.get(i).isAlive()) {
				synchronized (this) {
					threadNumber++;
				}
				threadPool.get(i).start();
				break;
			}
			i++;
		}
	}
}

package window;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

import service.SendGet;

public class MainWindow {

	private JFrame frame;
	private JTextField textFieldPage;
	private JTextField textFieldKeyword;
	private JTextField threadField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainWindow() {
		initialize();
	}

	private String choseFile() {
		int result = 0;
		String path = null;
		JFileChooser fileChooser = new JFileChooser();
		FileSystemView fsv = FileSystemView.getFileSystemView(); // 注意了，这里重要的一句
		System.out.println(fsv.getHomeDirectory()); // 得到桌面路径
		fileChooser.setCurrentDirectory(fsv.getHomeDirectory());
		fileChooser.setDialogTitle("请选择要导出的文件名");
		fileChooser.setApproveButtonText("确定");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		result = fileChooser.showOpenDialog(frame);
		if (JFileChooser.APPROVE_OPTION == result) {
			path = fileChooser.getSelectedFile().getPath();
			return path + ".xlsx";
		}else {
			return null;
		}
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 257, 169);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton button = new JButton("开始");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String path;
					if ((path = choseFile()) == null)
						return;
					new SendGet().startGetInfoToExcel(textFieldKeyword.getText(), Integer.parseInt(textFieldPage.getText()), path, frame, Integer.parseInt(threadField.getText()));
				} catch (NumberFormatException | InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(78, 102, 84, 23);
		frame.getContentPane().add(button);

		JLabel label = new JLabel("关键字：");
		label.setBounds(10, 10, 84, 30);
		frame.getContentPane().add(label);
		label.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel label_1 = new JLabel("条数：");
		label_1.setBounds(10, 37, 84, 30);
		frame.getContentPane().add(label_1);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);

		textFieldPage = new JTextField();
		textFieldPage.setBounds(104, 42, 127, 21);
		frame.getContentPane().add(textFieldPage);
		textFieldPage.setColumns(1);

		textFieldKeyword = new JTextField();
		textFieldKeyword.setBounds(104, 15, 127, 21);
		frame.getContentPane().add(textFieldKeyword);
		textFieldKeyword.setColumns(1);
		
		JLabel label_2 = new JLabel("\u7EBF\u7A0B\u6570\uFF1A");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setBounds(10, 62, 84, 30);
		frame.getContentPane().add(label_2);
		
		threadField = new JTextField();
		threadField.setText("10");
		threadField.setColumns(1);
		threadField.setBounds(104, 67, 127, 21);
		frame.getContentPane().add(threadField);
	}
}
